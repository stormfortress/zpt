#include <RoboCatServerPCH.h>

MouseServer::MouseServer()
{
}

void MouseServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}

void MouseServer::Update()
{
	//Mouse::Update();

	/*
	if (Timing::sInstance.GetFrameStartTime() > mTimeToDie)
	{
		SetDoesWantToDie(true);
	}
	*/
}

bool MouseServer::HandleCollisionWithCat( RoboCat* inCat )
{
	//kill yourself!
	SetDoesWantToDie(true);

	static_cast< RoboCatServer* >(inCat)->TakeDamage(GetPlayerId(), 1);

	return false;

	/*John's original code
	//kill yourself!
	SetDoesWantToDie( true );

	ScoreBoardManager::sInstance->IncScore( inCat->GetPlayerId(), 1 );

	return false;
	*/

	/*  Collision method from YarnServer
	bool YarnServer::HandleCollisionWithCat( RoboCat* inCat )
	{
		if( inCat->GetPlayerId() != GetPlayerId() )
		{
			//kill yourself!
			SetDoesWantToDie( true );

			static_cast< RoboCatServer* >( inCat )->TakeDamage( GetPlayerId() );

		}

		return false;
	}*/
}