#include <RoboCatServerPCH.h>

//uncomment this when you begin working on the server

bool Server::StaticInit()
{
	sInstance.reset(new Server());

	return true;
}

Server::Server()
{
	GameObjectRegistry::sInstance->RegisterCreationFunction('RCAT', RoboCatServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('MOUS', MouseServer::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction('YARN', YarnServer::StaticCreate);

	InitNetworkManager();
	
	//NetworkManagerServer::sInstance->SetDropPacketChance( 0.8f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.25f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.5f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.1f );
}

int Server::Run()
{
	SetupWorld();

	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	string portString = StringUtils::GetCommandLineArg(1);
	uint16_t port = stoi(portString);

	return NetworkManagerServer::StaticInit(port);
}

void Server::CreateRandomMice(int inMouseCount)
{
	Vector3 mouseMin(-5.f, -3.f, 0.f);
	Vector3 mouseMax(5.f, 3.f, 0.f);
	GameObjectPtr go;

	//make a mouse somewhere- where will these come from?
	for(int i = 0; i < inMouseCount; ++i)
	{
		/*
		This is all of John's original code that was in this loop

		go = GameObjectRegistry::sInstance->CreateGameObject('MOUS');
		Vector3 mouseLocation = RoboMath::GetRandomVector(mouseMin, mouseMax);
		go->SetLocation(mouseLocation);
		*/

		go = GameObjectRegistry::sInstance->CreateGameObject('MOUS');
		Vector3 mouseLocation = RoboMath::GetRandomVector(mouseMin, mouseMax);

		mouseLocation.mX = 1;
		mouseLocation.mY = 1;

		if (i == 0)
		{
			mouseLocation.mX = -4;
			mouseLocation.mY = -2;
		}

		else if (i == 1)
		{
			mouseLocation.mX = -3;
			mouseLocation.mY = 3;
		}

		else if (i == 2)
		{
			mouseLocation.mX = -2;
			mouseLocation.mY = -2;
		}

		else if (i == 3)
		{
			mouseLocation.mX = -1;
			mouseLocation.mY = 3;
		}

		else if (i == 4)
		{
			mouseLocation.mX = 0;
			mouseLocation.mY = -2;
		}

		else if (i == 5)
		{
			mouseLocation.mX = 1;
			mouseLocation.mY = 3;
		}

		else if (i == 6)
		{
			mouseLocation.mX = 2;
			mouseLocation.mY = -2;
		}

		else if (i == 7)
		{
			mouseLocation.mX = 3;
			mouseLocation.mY = 3;
		}

		else if (i == 8)
		{
			mouseLocation.mX = 4;
			mouseLocation.mY = -2;
		}

		else if (i == 9)
		{
			mouseLocation.mX = 5;
			mouseLocation.mY = 3;
		}

		go->SetLocation(mouseLocation);
	}
}

void Server::SetupWorld()
{
	//spawn some random mice
	CreateRandomMice(10);
}

void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnCats();

	Engine::DoFrame();

	NetworkManagerServer::sInstance->SendOutgoingPackets();
}

void Server::HandleNewClient(ClientProxyPtr inClientProxy)
{
	int playerId = inClientProxy->GetPlayerId();
	
	ScoreBoardManager::sInstance->AddEntry(playerId, inClientProxy->GetName());
	SpawnCatForPlayer(playerId);
}

void Server::SpawnCatForPlayer(int inPlayerId)
{
	RoboCatPtr cat = std::static_pointer_cast<RoboCat>(GameObjectRegistry::sInstance->CreateGameObject('RCAT'));
	cat->SetColor(ScoreBoardManager::sInstance->GetEntry(inPlayerId)->GetColor());
	cat->SetPlayerId(inPlayerId);
	//gotta pick a better spawn location than this...

	//This is John's original code' below.  For the time being it starts the player at the left hand side.  Improvements for this would be to rotate the image so that it's pointing to the right.
	//cat->SetLocation(Vector3(1.f - static_cast<float>(inPlayerId), 0.f, 0.f));

	cat->SetLocation(Vector3(-6.f, 0.f - static_cast<float>(inPlayerId), 0.f));
}

void Server::HandleLostClient(ClientProxyPtr inClientProxy)
{
	//kill client's cat
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();

	ScoreBoardManager::sInstance->RemoveEntry(playerId);
	RoboCatPtr cat = GetCatForPlayer(playerId);
	if(cat)
	{
		cat->SetDoesWantToDie(true);
	}
}

RoboCatPtr Server::GetCatForPlayer(int inPlayerId)
{
	//run through the objects till we find the cat...
	//it would be nice if we kept a pointer to the cat on the clientproxy
	//but then we'd have to clean it up when the cat died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i)
	{
		GameObjectPtr go = gameObjects[i];
		RoboCat* cat = go->GetAsCat();

		if(cat && cat->GetPlayerId() == inPlayerId)
		{
			return std::static_pointer_cast<RoboCat>(go);
		}
	}

	return nullptr;
}