#include <RoboCatClientPCH.h>
#include "SDL_image.h"
std::unique_ptr< GraphicsDriver >	GraphicsDriver::sInstance;

namespace
{
}


bool GraphicsDriver::StaticInit( SDL_Window* inWnd )
{
	GraphicsDriver* newGraphicsDriver = new GraphicsDriver();
	bool result = newGraphicsDriver->Init( inWnd );

	if( !result )
	{
		delete newGraphicsDriver;
	}
	else
	{
		sInstance.reset( newGraphicsDriver );
	}

	return result;
}

bool GraphicsDriver::Init( SDL_Window* inWnd )
{
	mRenderer = SDL_CreateRenderer( inWnd, -1, SDL_RENDERER_ACCELERATED );
	SDL_Renderer* gRenderer = SDL_CreateRenderer(inWnd, 2000, SDL_RENDERER_ACCELERATED);
	if( mRenderer == nullptr )
	{
		SDL_LogError( SDL_LOG_CATEGORY_ERROR, "Failed to create hardware-accelerated renderer." );
		return false;
	}
	//Trying to add background image
	//https://wiki.libsdl.org/SDL_RenderCopy
	SDL_Surface* SeaFloor = IMG_Load("../Assets/seafloor.png");
	SDL_Texture* tex = SDL_CreateTextureFromSurface(mRenderer, SeaFloor);
	SDL_FreeSurface(SeaFloor);
	//might need location data?
	//test
	SrcR.x = 1280;
	SrcR.y = 720;
	
	
	DstR.x = 0;
	DstR.y = 0;
    //end test
	 //Cornflower blue background, cause why not?
	SDL_RenderCopy(mRenderer, tex, NULL, NULL);
	SDL_SetRenderDrawColor( mRenderer, 12, 77, 105, SDL_ALPHA_OPAQUE );
	// Set the logical size to 1280x720 so everything will just auto-scale
	SDL_RenderSetLogicalSize( mRenderer, 1280, 720 );
	SDL_RenderSetLogicalSize(mRenderer, 1280, 720);
	SDL_RenderPresent(mRenderer);
	SDL_Delay(500);
	return true;
	//if background image fails, will at least change horrid color...
}

GraphicsDriver::GraphicsDriver()
	: mRenderer( nullptr )
{
}


GraphicsDriver::~GraphicsDriver()
{
	if( mRenderer != nullptr )
	{
		SDL_DestroyRenderer( mRenderer );
	}
}

void GraphicsDriver::Clear()
{
	SDL_RenderClear( mRenderer );
}

void GraphicsDriver::Present()
{
	SDL_RenderPresent( mRenderer );
}

SDL_Rect& GraphicsDriver::GetLogicalViewport()
{
	SDL_RenderGetLogicalSize( mRenderer, &mViewport.w, &mViewport.h );

	return mViewport;
}

SDL_Renderer* GraphicsDriver::GetRenderer()
{
	return mRenderer;
}
